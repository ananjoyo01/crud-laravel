<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class MainController extends Controller
{
	public function index()
	{
		$karyawan = DB::table('karyawan')->get();
		return view('index',['karyawan' => $karyawan]);

	}

	public function tambah()
	{
		return view('tambah');
	}

	public function add(Request $request)
	{
		DB::table('karyawan')->insert([
			'nama_karyawan' => $request->nama,
			'no_karyawan' => $request->no,
			'no_telp_karyawan' => $request->telp,
			'jabatan_karyawan' => $request->jabatan,
			'divisi_karyawan' => $request->divisi
		]);
		return redirect('/');
	}

	public function edit($id)
	{
		$karyawan = DB::table('karyawan')->where('id_karyawan',$id)->get();
		return view('edit',['karyawan' => $karyawan]);
	}

	public function update(Request $request)
	{
		DB::table('karyawan')->where('id_karyawan',$request->id)->update([
			'nama_karyawan' => $request->nama,
			'no_karyawan' => $request->no,
			'no_telp_karyawan' => $request->telp,
			'jabatan_karyawan' => $request->jabatan,
			'divisi_karyawan' => $request->divisi,
		]);
		return redirect('/');
	}

	public function hapus($id)
	{
		DB::table('karyawan')->where('id_karyawan',$id)->delete();
		return redirect('/');
	}
}
